﻿using Homework.ThirdParty;
using Moq;
using NUnit.Framework;
using System;

namespace Homework.UnitTests
{
    [TestFixture]
    public class AccountTests
    {
        private Account _account;

        private Mock<IAction> _mockAction;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {

        }

        [TearDown]
        public void TearDown()
        {

        }

        [SetUp]
        public void SetUp()
        {
            _mockAction = new Mock<IAction>();
            _account = new Account(0);
        }

        [Test]
        [Category("RegisterTest")]
        public void Unregistered_Register_SuccessfullyRegistered()
        {
            //Arrange
            bool expectedResult = true;

            //Act
            _account.Register();

            //Assert
            Assert.That(_account.IsRegistered, Is.EqualTo(expectedResult));
            Assert.That(_account.IsRegistered, Is.True);
        }

        [Test]
        [Category("RegisterTest")]
        public void Registered_Register_UnsuccessfullyRegistered()
        {
            //Arrange
            _account.Register();

            //Act
            //Assert
            Assert.That(() => _account.Register(), Throws.TypeOf<AccountAlreadyExistsException>());
        }

        [Test]
        [Category("ActivateTest")]
        public void Deactivated_Activate_SuccessfullyActivated()
        {
            //Arrange
            bool expectedResult = true;

            //Act
            _account.Activate();

            //Assert
            Assert.That(_account.IsConfirmed, Is.EqualTo(expectedResult));
            Assert.That(_account.IsConfirmed, Is.True);
        }

        [Test]
        [Category("ActivateTest")]
        public void Activated_Activate_UnsuccessfullyActivated()
        {
            //Arrange
            _account.Activate();

            //Act
            //Assert
            Assert.That(() => _account.Activate(), Throws.TypeOf<AccountAlreadyActivatedException>());
        }

        [Test]
        [Category("ActionTest")]
        public void ActionIsNotExecutable_PerformAction_UnsuccessfullyPerformedAction()
        {
            //Arrange
            _account.ActionsSuccessfullyPerformed = 0;
            _mockAction.Setup(action => action.Execute()).Returns(false);

            int expectedNumberOfActionsSuccessfullyPerformed = 0;

            //Act
            _account.PerformAction(_mockAction.Object);

            //Assert

            Assert.That(_account.ActionsSuccessfullyPerformed,
                Is.EqualTo(expectedNumberOfActionsSuccessfullyPerformed));

            _mockAction.Verify(account => account.Execute());
            _mockAction.VerifyNoOtherCalls();
        }

        [Test]
        [Category("PerformActionTest")]
        public void ActionIsExecutable_PerformAction_SuccessfullyPerformedAction()
        {
            //Arrange
            _account.ActionsSuccessfullyPerformed = 0;
            _mockAction.Setup(action => action.Execute()).Returns(true);

            int expectedNumberOfActionsSuccessfullyPerformed = 1;

            //Act
            _account.PerformAction(_mockAction.Object);

            //Assert

            Assert.That(_account.ActionsSuccessfullyPerformed,
                Is.EqualTo(expectedNumberOfActionsSuccessfullyPerformed));

            _mockAction.Verify(account => account.Execute());
            _mockAction.VerifyNoOtherCalls();
        }

        [Test]
        [Category("TakeAction")]
        public void Validated_TakeAction_UnsuccessfulLyPerformedAction()
        {
            _mockAction.Setup(action => action.Execute()).Returns(false);

            _account.Activate();
            _account.Register();

            int expectedNumberOfActionsSuccessfullyPerformed = 0;

            //Act
            _account.TakeAction(_mockAction.Object);

            //Assert
            Assert.That(_account.IsConfirmed, Is.True);
            Assert.That(_account.IsRegistered, Is.True);
            Assert.That(_account.ActionsSuccessfullyPerformed,
                Is.EqualTo(expectedNumberOfActionsSuccessfullyPerformed));

            _mockAction.Verify(account => account.Execute());
            _mockAction.VerifyNoOtherCalls();
        }

        [Test]
        [Category("TakeAction")]
        public void Validated_TakeAction_SuccessfulLyPerformedAction()
        {
            _mockAction.Setup(action => action.Execute()).Returns(true);

            _account.Activate();
            _account.Register();

            int expectedNumberOfActionsSuccessfullyPerformed = 1;

            //Act
            _account.TakeAction(_mockAction.Object);

            //Assert
            Assert.That(_account.IsConfirmed, Is.True);
            Assert.That(_account.IsRegistered, Is.True);
            Assert.That(_account.ActionsSuccessfullyPerformed,
                Is.EqualTo(expectedNumberOfActionsSuccessfullyPerformed));

            _mockAction.Verify(account => account.Execute());
            _mockAction.VerifyNoOtherCalls();
        }

        [Test]
        [Category("StatusCheck")]
        public void RegisteredAndConfirmed_CheckIfInactive_ReturnsActive()
        {
            _account.Activate();
            _account.Register();

            //Act
            bool isInactive =_account.IsInactiveAccount();

            //Assert
            Assert.That(isInactive, Is.False);
        }

        [Test]
        [Category("StatusCheck")]
        public void UnregisteredAndInconfirmed_CheckIfInactive_ReturnsActive()
        {
            //Act
            bool isInactive = _account.IsInactiveAccount();

            //Assert
            Assert.That(isInactive, Is.True);
        }

        [Test]
        [Category("ValidateAccount")]
        public void AccountNotValidated_ValidateAccount_ThrowsInactiveUserException()
        {
            //Arrange
            //Act
            //Assert
            Assert.That(_account.IsRegistered, Is.False);
            Assert.That(_account.IsConfirmed, Is.False);
            Assert.That(() => _account.ValidateAccount(), Throws.TypeOf<InactiveUserException>());
        }
    }
}
