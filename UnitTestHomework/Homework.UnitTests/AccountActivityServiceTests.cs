﻿using Homework.ThirdParty;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Homework.UnitTests
{
    [TestFixture]
    public class AccountActivityServiceTests
    {
        private AccountActivityService _accountActivityService;
        private IAccountRepository _fakeRepository;
        private Mock<IAccountRepository> _mockRepository;
        private List<Account> accounts;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            accounts = new List<Account>()
            {
                new Account(0) { ActionsSuccessfullyPerformed = 0 },
                new Account(1) { ActionsSuccessfullyPerformed = 1 },
                new Account(2) { ActionsSuccessfullyPerformed = 21 },
                new Account(3) { ActionsSuccessfullyPerformed = 41 },
                new Account(4) { ActionsSuccessfullyPerformed = 1},
                new Account(5) { ActionsSuccessfullyPerformed = 21},
                new Account(6) { ActionsSuccessfullyPerformed = 1}
            };
        }

        [SetUp]
        public void SetUp()
        {
            _fakeRepository = new FakeAccountRepository();
            SetUpFakeRepository();
            _accountActivityService = new AccountActivityService(_fakeRepository);
            _mockRepository = new Mock<IAccountRepository>();
        }

        private void SetUpFakeRepository()
        {
            foreach (Account account in accounts)
            {
                _fakeRepository.Add(account);
            }
        }

        [Category("GetActivity")]
        [TestCase(0, ActivityLevel.None)]
        [TestCase(1, ActivityLevel.Low)]
        [TestCase(2, ActivityLevel.Medium)]
        [TestCase(3, ActivityLevel.High)]
        public void ExistingIdGiven_GetActivity_ReturnsCorrectValue(int accountId, ActivityLevel expectedResult)
        {
            //Act
            ActivityLevel actualResult = _accountActivityService.GetActivity(accountId);

            //Assert
            Assert.That(actualResult, Is.EqualTo(expectedResult));

        }

        [TestCase(100)]
        public void InvalidIdGiven_GetActivity_ThrowException(int accountId)
        {
            //arrange
            //act
            //assert
            Assert.That(() => _accountActivityService.GetActivity(accountId),
                Throws.TypeOf<AccountNotExistsException>());
        }

        [Category("GetAmountForActivity")]
        [TestCase(ActivityLevel.None, 1)]
        [TestCase(ActivityLevel.Low, 3)]
        [TestCase(ActivityLevel.Medium, 2)]
        [TestCase(ActivityLevel.High, 1)]
        public void ValidAcitvityLevelGiven_GetAmountForActivity_GetsRightAmountOfActivity(ActivityLevel activityLevel, int expectedCount)
        {
            //arrange (Setup gets it done)
            //act

            int actualCount = _accountActivityService.GetAmountForActivity(activityLevel);

            //assert
            Assert.That(actualCount, Is.EqualTo(expectedCount));
            Assert.That(actualCount, Is.Not.Null);
        }

        [Category("ServiceRepositoryInteractionTest")]
        [TestCase(ActivityLevel.None, 1)]
        [TestCase(ActivityLevel.Low, 3)]
        [TestCase(ActivityLevel.Medium, 2)]
        [TestCase(ActivityLevel.High, 1)]
        public void ValidActivityLevelGiven_GetAmountOfActivity_VerifiesRepositoryGetAll(ActivityLevel activityLevel, int expectedCount)
        {
            //Arrange
            _mockRepository.Setup(repo => repo.GetAll());
            _accountActivityService = new AccountActivityService(_mockRepository.Object);
            
            //Act
            int actualCount = _accountActivityService.GetAmountForActivity(activityLevel);
            
            //Assert
            _mockRepository.Verify(repo => repo.GetAll());
            _mockRepository.VerifyNoOtherCalls();
        }

        [Category("ServiceRepositoryInteractionTest")]
        [TestCase(0, ActivityLevel.None)]
        [TestCase(1, ActivityLevel.Low)]
        [TestCase(2, ActivityLevel.Medium)]
        [TestCase(3, ActivityLevel.High)]
        public void ExistingIdGiven_GetActivity_VerifiesRepositoryGet(int accountId, ActivityLevel expectedResult)
        {
            //Arrange
            _mockRepository.Setup(repo => repo.Get(It.IsAny<int>()))
                .Returns(It.IsAny<Account>());
            _accountActivityService = new AccountActivityService(_mockRepository.Object);

            //Act
            try
            {
                ActivityLevel actualResult = _accountActivityService.GetActivity(accountId);
            }
            catch (Exception)
            {
            }

            //Assert
            _mockRepository.Verify(repo => repo.Get(accountId));
            _mockRepository.VerifyNoOtherCalls();
        }
    }
}
