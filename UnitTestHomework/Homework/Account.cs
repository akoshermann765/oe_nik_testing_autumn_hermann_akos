﻿ using Homework.ThirdParty;

namespace Homework
{
	public class Account
	{
		public int Id { get; }

		public bool IsRegistered { get; private set; }
		public bool IsConfirmed { get; private set; }

		public int ActionsSuccessfullyPerformed { get; set; }

		public Account(int id)
		{
			Id = id;
		}

		public void Register()
		{
			if (IsRegistered)
            {
				throw new AccountAlreadyExistsException();
            }
			IsRegistered = true;
		}

		public void Activate()
		{
			if (IsConfirmed)
            {
				throw new AccountAlreadyActivatedException();
            }
			IsConfirmed = true;
		}

		public bool TakeAction(IAction action)
		{
			ValidateAccount();
			return PerformAction(action);
		}


		public bool PerformAction(IAction action)
		{
			bool success = action.Execute();
			if (success)
			{
				ActionsSuccessfullyPerformed++;
			}
			return success;
		}

		public void ValidateAccount()
		{
			if (IsInactiveAccount())
			{
				throw new InactiveUserException();
			}
		}

		public bool IsInactiveAccount()
		{
			return IsNotRegistered() && IsNotConfirmed();
		}

		private bool IsNotRegistered()
		{
			return !IsRegistered;
		}

		private bool IsNotConfirmed()
		{
			return !IsConfirmed;
		}
	}
}