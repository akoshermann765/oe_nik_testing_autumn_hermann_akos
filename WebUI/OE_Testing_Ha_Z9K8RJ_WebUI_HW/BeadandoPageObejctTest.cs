﻿using NUnit.Framework;
using OE_Testing_Ha_Z9K8RJ_WebUI_HW.Enums;
using OE_Testing_Ha_Z9K8RJ_WebUI_HW.PageObjects;
using OpenQA.Selenium;
using System;
using System.Collections;
using System.Linq;
using System.Xml.Linq;

namespace OE_Testing_Ha_Z9K8RJ_WebUI_HW
{
    public class BeadandoPageObejctTest : TestBase
    {
        public static int testNumbering = 0; // A tesztek számát tartja nyilván (3. teszt szándékosan fail-el)
        public static int screenshotNumbering = 0; //Segédszámláló screenshotok fájlnevéhez

        //[TestCase(Category.Sport, "Logo found")]
        //[TestCase(Category.Film, "Film")]
        //[TestCase(Category.TV, "TÉVÉ")]
        [TestCaseSource("LoadTestData")]
        public void TestSubPages(Category category, string headertext)
        {
            //Arrrange
            testNumbering++;

            string expected = headertext.ToUpper(); // Elkerülendő a kis- és nagybetűk eltérése kapcsán fellépő hibák.
            string actual = String.Empty;

            //Act
            var testedPage = HomePage
                .Navigate(Driver)
                .GetHomePageWidget()
                .SelectCategory(category.ToString()); //Chain of methods

            switch (category) // A GetHearName leválasztásra került, hogy a megfelelő leszármazott függvénye kerüljön meghívásra.
            {
                case Category.Film:
                    actual = (testedPage as FilmWidget).GetHeaderName();
                    break;

                case Category.TV:
                    actual = (testedPage as TelevisionWidget).GetHeaderName();
                    break;

                case Category.Sport:
                    actual = (testedPage as SportsWidget).GetHeaderName();
                    break;
            }


            //Assert
            Assert.AreEqual(expected, actual.ToUpper()); 

            if (testNumbering >= 3) //A harmadik teszt szándékosan elbukik, erről screenshot készül.
            {
                Screenshot();
                Assert.Fail("Test failed on purpouse...");
            }
                
        }

        //Screenshotot készít a screenshots mappába, a fájlnévben tartalmazva, hogy hanyadikról screenshotról van szó a futtatás során, valamint az aktuális dátumot.
        private void Screenshot()
        {
            string currentDate = DateTime.Now.ToString("YYYY-MM-DD_HH-mm-ss");
            Screenshot screenshot = (Driver as ITakesScreenshot).GetScreenshot();
            screenshot.SaveAsFile("screenshots/TestError_"
                + "_" + ++screenshotNumbering
                + currentDate
                + ".png", ScreenshotImageFormat.Png);
        }

        //Betölti a tesztadatok egy XML fájlból, majd egy névtelen objektummal tér vissza, amit a teszt felhasznál.
        private static IEnumerable LoadTestData()
        {
            XElement doc = XElement.Load("test_data.xml");
            Type enumType = typeof(Category);
            return from attributes in doc.Descendants("subpage")
                   let category = Enum.Parse(enumType, attributes.Attribute("category").Value, true) // A stringet enummá konvertálni szükséges
                   let headertext = attributes.Attribute("headertext").Value
                   select new object[] { category, headertext };
        }
    }
}
