﻿using OE_Testing_Ha_Z9K8RJ_WebUI_HW.Enums;
using OE_Testing_Ha_Z9K8RJ_WebUI_HW.PageObjects;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;

namespace OE_Testing_Ha_Z9K8RJ_WebUI_HW.PageObjects
{
    class HomePageWidget : WidgetBase
    {
        //Cookie-kal kapcsolatos elemek
        public IWebElement CookieQuestionWindow => Driver.FindElement(By.CssSelector("div[class='fc-dialog fc-choice-dialog']"));
        public IWebElement AcceptCookiesButton => Driver.FindElement(By.CssSelector("button[class='fc-button fc-cta-consent fc-primary-button']"));

        //public IEnumerable<IWebElement> AllElements => Driver.FindElements(By.CssSelector("a[class='omi-link']"));

        //public IWebElement FilmLi => Driver.FindElement(By.CssSelector("li[class='om-item omi-sport']"));
        //public IWebElement FilmBtn => FilmLi.FindElement(By.CssSelector("a[class='omi-link']"));

        //Rovatok gomb elemi
        public IWebElement SportsButton => Driver.FindElement(By.CssSelector("li[class='om-item omi-sport']"))
            .FindElement(By.CssSelector("a[class='omi-link']"));
        public IWebElement TVButton => Driver.FindElement(By.CssSelector("li[class='om-item omi-teve']"))
            .FindElement(By.CssSelector("a[class='omi-link']"));
        public IWebElement FilmButton => Driver.FindElement(By.CssSelector("li[class='om-item omi-filmklub']"))
            .FindElement(By.CssSelector("a[class='omi-link']"));

        public HomePageWidget(IWebDriver webDriver) : base(webDriver) { }

        //Kategóriaválasztás
        public WidgetBase SelectCategory(string category)
        {
            if (category == "Sport")
            {
                return GoTo(SportsButton, typeof(SportsWidget));
            }

            if (category == "TV")
            {
                return GoTo(TVButton, typeof(TelevisionWidget));
            }

            if(category == "Film")
            {
                return GoTo(FilmButton, typeof(FilmWidget));
            }

            return new WidgetBase(Driver);
        }

        //public WidgetBase GoToSports()
        //{
        //    WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
        //    wait.Until(t => SportsButton.Displayed);

        //    SportsButton.Click();
        //    return Activator.CreateInstance(typeof(SportsWidget), Driver) as SportsWidget;
        //}

        //public WidgetBase GoToTelevision()
        //{
        //    if (AcceptCookies())
        //    {
        //        WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
        //        wait.Until(t => TVButton.Displayed);

        //        TVButton.Click();
        //        return Activator.CreateInstance(typeof(TelevisionWidget), Driver) as TelevisionWidget;
        //    }
        //    return null;
        //}

        public WidgetBase GoTo(IWebElement button, Type pageType)
        {
            if (AcceptCookies())
            {
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(15));
                wait.Until(t => button.Displayed);

                button.Click();
                return Activator.CreateInstance(pageType, Driver) as WidgetBase;
            }

            return new WidgetBase(Driver);
        }

        // Sütik elfogadása
        private bool AcceptCookies()
        {
            Driver.Manage().Window.FullScreen(); //Teljesképernyős mód

            //Explicit várakozás a sütiablak felugrásáig
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            wait.Until(t => CookieQuestionWindow.Displayed);

            if (AcceptCookiesButton.Displayed) //Ha megjelent, elfogadjuk és igaz értékkel térünk vissza
            {
                AcceptCookiesButton.Click();
                return true;
            }

            return false;
        }
    }
}
