﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;

namespace OE_Testing_Ha_Z9K8RJ_WebUI_HW.PageObjects
{
    class FilmWidget : WidgetBase
    {
        //Cím elem
        IWebElement HeaderElement => Driver.FindElement(By.ClassName("opt-title"));

        public FilmWidget(IWebDriver webDriver) : base(webDriver) { }

        public override string GetHeaderName()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            wait.Until(t => HeaderElement.Displayed);
            return HeaderElement.Displayed ? HeaderElement.Text : String.Empty; //Ha megjelent, viszaadjuk a szövegét.
        }
    }
}
