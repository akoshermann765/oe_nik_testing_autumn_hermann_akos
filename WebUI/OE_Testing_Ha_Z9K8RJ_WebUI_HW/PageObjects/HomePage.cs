﻿using OE_Testing_Ha_Z9K8RJ_WebUI_HW.PageObjects;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace OE_Testing_Ha_Z9K8RJ_WebUI_HW.PageObjects
{
    class HomePage : BasePage
    {
        public HomePage(IWebDriver webDriver) : base(webDriver) { }

        public static HomePage Navigate(IWebDriver webDriver)
        {
            webDriver.Url = "https://www.origo.hu/index.html"; //A projekt az origo oldalán navigál
            return new HomePage(webDriver);
        }

        public HomePageWidget GetHomePageWidget()
        {
            return new HomePageWidget(Driver);
        }
    }
}
