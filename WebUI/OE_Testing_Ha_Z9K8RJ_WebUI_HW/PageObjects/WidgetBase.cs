﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;

namespace OE_Testing_Ha_Z9K8RJ_WebUI_HW.PageObjects
{
    //A BasePage osztályhoz hasonlóan a widgetek is rendelkeznek egy ősosztállyal:
    //A GetHeaderName virutális teszi elhetővé a leszármazottaknak, hogy visszaadják az adott subpage header-jét.
    class WidgetBase
    {
        protected IWebDriver Driver;

        public WidgetBase(IWebDriver webDriver)
        {
            Driver = webDriver;
        }

        public virtual string GetHeaderName()
        {
            return String.Empty;
        }
    }
}
