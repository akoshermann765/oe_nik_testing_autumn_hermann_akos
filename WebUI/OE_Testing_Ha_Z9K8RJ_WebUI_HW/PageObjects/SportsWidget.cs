﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;

namespace OE_Testing_Ha_Z9K8RJ_WebUI_HW.PageObjects
{
    class SportsWidget : WidgetBase
    {
        public SportsWidget(IWebDriver webDriver) : base(webDriver) { }

        // A sport rovatnak nincs headerje, egy logó jelenik meg az oldal tetején.
        IWebElement OrigoSportsLogo => Driver?.FindElement(By.CssSelector("a[class='os-logo']"));

        public override string GetHeaderName()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            wait.Until(t => OrigoSportsLogo.Displayed);
            return OrigoSportsLogo != null ? "Logo found" : "Not found"; //Amennyiben a logo megjelent az oldalon, úgy a megadott szöveget adja vissza a fgv.
        }
    }
}
