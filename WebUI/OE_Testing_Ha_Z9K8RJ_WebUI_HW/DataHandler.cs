﻿using OE_Testing_Ha_Z9K8RJ_WebUI_HW.Enums;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace OE_Testing_Ha_Z9K8RJ_WebUI_HW
{
    public static class DataHandler
    {
        //Adatkezelő osztály, végül nem került felhasználásra

        public static string filename = "test_data.xml";

        public static IEnumerable LoadDataSource()
        {
            XElement doc = XElement.Load(filename);
            Type enumType = typeof(Category);
            return from attributes in doc.Descendants("Page")
                   let category = Enum.Parse(enumType, attributes.Attribute("").Value)
                   let headertext = attributes.Attribute("").Value
                   select new object[] { category, headertext };
        }
    }
}
