*******Hermann Ákos (Z9K8RJ) - Web UI házi feladat**********


A projekt az origo.hu oldalára navigál el, majd választ ki három darab általam választott rovatot: Sport, TV, Film.
A megjelenést vizsgáló teszt a TV és Film aloldalak esetében a cím (header) megjelenítését vizsgálja, míg a
Sportrovat esetén csupán egy logó jelenik meg. Ezen oldal esetén a logó jelenléte kerül vizsgálatra. (Egy saját szöveget
ad vissza az idevágó fgv., ha megjelenik a logo)

A tesztesetek paramétereiként szolgáló adatok egy test_data nevű XML fájlból érkeznek, ezek kerülnek beolvasásra egy függvény
segíéségével.

A harmadikként lefutó teszt szándékosan elbukik (Assert.Fail), az oldal állapotáról ezt megelőzően pedig egy screenshot
készül az aktuális idővel és egy ID-val a nevében.

A megvalósítás során igyekeztem felhasználni a videókban bemutatott technikákat és best practice-eket.